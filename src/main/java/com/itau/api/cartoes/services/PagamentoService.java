package com.itau.api.cartoes.services;


import com.itau.api.cartoes.DTOs.PagamentoDTO;
import com.itau.api.cartoes.models.Cartao;
import com.itau.api.cartoes.models.Pagamento;
import com.itau.api.cartoes.models.Status;
import com.itau.api.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoService cartaoService;


    public Pagamento registrarPagamento(PagamentoDTO pagamentoDTO) throws Exception {

        Cartao cartao = cartaoService.buscarCartao(pagamentoDTO.getNumero());

        if(cartao == null)
        {
            throw new RuntimeException("Número do cartão informado é inválido");
        }

        if (!pagamentoDTO.getNumero().equals(cartao.getNumero())) {

            throw new RuntimeException("Número do cartão informado é inválido");
        } else if (cartao.getStatus() == Status.Cancelado) {
            throw new RuntimeException("Cartão Cancelado, favor informar um cartão valido para compra!");

        }

        Pagamento pagamento = pagamentoDTO.converterParaPagamento();
        pagamento.setCartao(cartao);

        return pagamentoRepository.save(pagamento);
    }

}
