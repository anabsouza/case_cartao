package com.itau.api.cartoes.services;


import com.itau.api.cartoes.DTOs.CartaoDTO;
import com.itau.api.cartoes.models.Cartao;
import com.itau.api.cartoes.models.Cliente;
import com.itau.api.cartoes.models.Status;
import com.itau.api.cartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Optional;

import java.time.LocalDate;
import java.util.Random;


@Service
public class CartaoService {

    //**** Cria/Salva o Cartao
    @Autowired
    private CartaoRepository cartaoRepository;

    public Cartao salvar(Cartao cartao) {

        return cartaoRepository.save(cartao);
    }

    //**** Busca o Cartao pelo ID
    public Cartao buscarCartaoPeloId(int id) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        if (cartaoOptional.isPresent()) {
            Cartao cartao = cartaoOptional.get();
            return cartao;
        } else {
            throw new RuntimeException("O numero do cartao não foi encontrado");
        }
    }

    //**** Cancela o cartão - mudando o status para C - cancelado
    public Cartao ativarCartao(int id, Boolean status) {
        Cartao statuscrt = buscarCartaoPeloId(id);

        if (!status)
            statuscrt.setStatus(Status.Cancelado);
        else
            statuscrt.setStatus(Status.Ativo);
        return cartaoRepository.save(statuscrt);
    }

    public Cartao buscarCartao(String numero) {

        return cartaoRepository.findByNumero(numero);
    }

    public Cartao criar(CartaoDTO cartaoDTO) {
        Cartao cartao = new Cartao();

        Random random = new Random();

        cartao.setNumero(String.format("%04d", random.nextInt(9999)) + "." +
                String.format("%04d", random.nextInt(9999)) + "." +
                String.format("%04d", random.nextInt(9999)) + "." +
                String.format("%04d", random.nextInt(9999)));

        cartao.setStatus(Status.Ativo);

        return cartao;
        }
}
