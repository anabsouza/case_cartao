package com.itau.api.cartoes.repositories;

import com.itau.api.cartoes.models.Pagamento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PagamentoRepository  extends JpaRepository<Pagamento,Integer > {
}
