package com.itau.api.cartoes.repositories;

import com.itau.api.cartoes.models.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository  extends JpaRepository<Cliente, Integer> {

    Cliente findByCpf(String cpf);
}
