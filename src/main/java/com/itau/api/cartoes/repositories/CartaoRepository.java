package com.itau.api.cartoes.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import com.itau.api.cartoes.models.Cartao;

public interface CartaoRepository  extends JpaRepository<Cartao,Integer > {

    Cartao findByNumero(String numero);

}
