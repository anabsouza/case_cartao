package com.itau.api.cartoes.controllers;


import com.itau.api.cartoes.DTOs.CartaoDTO;
import com.itau.api.cartoes.models.Cartao;
import com.itau.api.cartoes.models.Cliente;
import com.itau.api.cartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/clientes")
public class ClienteController  {

    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody @Valid Cliente cliente){
          return  clienteService.cadastrarCliente(cliente);

    }

    @GetMapping("/{id}")
    public Cliente pesquisarPorId(@PathVariable(name = "id") int id){
        try{
            Cliente cliente = clienteService.buscarClientePeloId(id);
            return cliente;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/{id}/solicitarcartao")
    public Cartao solicitarCartao(@PathVariable(name = "id") Integer id, @RequestBody @Valid CartaoDTO cartao) {
        return clienteService.solicitarCartao(cartao, id);
    }

}
