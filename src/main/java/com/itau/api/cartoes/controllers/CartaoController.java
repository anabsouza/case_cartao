package com.itau.api.cartoes.controllers;

import com.itau.api.cartoes.DTOs.PagamentoDTO;
import com.itau.api.cartoes.models.Cartao;
import com.itau.api.cartoes.models.Pagamento;
import com.itau.api.cartoes.services.CartaoService;
import com.itau.api.cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    CartaoService cartaoService;

    @Autowired
    PagamentoService pagamentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cartao salvar(@RequestBody @Valid Cartao cartao) {
        return cartaoService.salvar(cartao);
    }


    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public Pagamento registrarpagamento(@RequestBody @Valid PagamentoDTO pagamentoDTO) throws Exception {
        try {

            Pagamento objPagamento = pagamentoService.registrarPagamento(pagamentoDTO);

            return objPagamento;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }
//buscar cartao pelo ID
   @GetMapping("/{id}")
    public Cartao pesquisarPorId(@PathVariable(name = "id") int id){
        try{
            Cartao cartao = cartaoService.buscarCartaoPeloId(id);
            return cartao;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}

