package com.itau.api.cartoes.DTOs;

import com.itau.api.cartoes.models.Pagamento;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PagamentoDTO {

    private String numero;

    @NotNull(message = "Descricao da compra não pode ser nulo ")
    @NotBlank(message ="Descricao da compra pode ser em branco.")
    private String descricao;

    @DecimalMin(value = "0", message = "Valor da compra deve ser maior ou igual a zero")
    @Digits(integer = 6, fraction = 2, message = "Valor da compra fora do padrão")
    private double valordacompra;


    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }



    public double getValorDaCompra() {
        return valordacompra;
    }

    public void setValordacompra(double valordacompra) {
        this.valordacompra = valordacompra;
    }


    public Pagamento converterParaPagamento(){
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(this.descricao);
        pagamento.setValordacompra(this.valordacompra);
        return pagamento;
    }
}
