package com.itau.api.cartoes.DTOs;

public class CartaoDTO {

    private int id;
    private String numero;
    private boolean ativo;

    public CartaoDTO(int id, String numero, boolean ativo) {
        this.id = id;
        this.numero = numero;
        this.ativo = ativo;
    }

}
