package com.itau.api.cartoes.DTOs;

import javax.validation.constraints.NotNull;

public class StatusCrtDTO {
    // captura O STATUS do cartão para Inativo
    @NotNull(message =  "Status não pode ser nulo")
    private Boolean statuscrt;

    public Boolean getStatusCrt() {
        return statuscrt;
    }

    public void setStatuscrt(Boolean statuscrt) {
        this.statuscrt = statuscrt;
    }
}
