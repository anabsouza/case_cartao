package com.itau.api.cartoes.models;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id; //conta cartão

    @NotNull(message =  "Nome do cliente não  pode ser nulo")
    @NotBlank(message = "Nome do cliente não pode ser vazio")
    private String nome;


    public Cliente() {
    }


    //Realacionamento de 1 -Cliente para muitos cartões.
    @OneToMany
    public List<Cartao> cartao;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    public List<Cartao> getCartao() {
        return cartao;
    }

    public void setCartao(List<Cartao> cartao) {
        this.cartao = cartao;
    }
}
