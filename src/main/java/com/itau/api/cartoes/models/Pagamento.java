package com.itau.api.cartoes.models;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;

    @Entity
    public class Pagamento {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private int id;

        @ManyToOne
        private  Cartao cartao;

        private String descricao;

        @DecimalMin(value = "0", message = "Valor da compra deve ser maior ou igual a zero")
        @Digits(integer = 6, fraction = 2, message = "Valor da compra fora do padrão")
        private double valordacompra;


        public Pagamento() {
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDescricao() {
            return descricao;
        }

        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }

        public Cartao getCartao() {
            return cartao;
        }

        public void setCartao(Cartao cartao) {
            this.cartao = cartao;
        }

        public void setValordacompra(double valordacompra) {
        }
    }

